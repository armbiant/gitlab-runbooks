# Teleport

**Table of Contents**

[TOC]

[Teleport](https://goteleport.com/docs/) is an *Access Management Platform*.

## Guides

- [Teleport Administration](./teleport_admin.md)
- [Teleport Approver Workflow](./teleport_approval_workflow.md)
- [Teleport Disaster Recovery](./teleport_disaster_recovery.md)
- [How to connect to a Rails Console using Teleport](./Connect_to_Rails_Console_via_Teleport.md)
- [How to connect to a Database console using Teleport](./Connect_to_Database_Console_via_Teleport.md)

## Architecture

The following diagram shows the Teleport architecture for GitLab infrastrucutre.
Some details are omitted for brevity.
Teleport resources, shown in green with Teleport icon, are not technically part of any Google Cloud projects.

![Click on the image to see the image in full size](./images/teleport-arch.png "GitLab Teleport Architecture")
