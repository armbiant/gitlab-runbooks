groups:
  - name: gitlab-walg-backup.rules
    rules:
      - record: gitlab_com:last_walg_backup_age_in_seconds
        expr: min(time() - walg_backup_last_completed_time_seconds{env="gstg"}) by (env,environment,type)
      - record: gitlab_com:last_walg_basebackup_age_in_hours
        expr: |
          min without (fqdn,instance) (
            time()
            -
            (
              gitlab_job_start_timestamp_seconds{resource="walg-basebackup", env="gstg"} > 0
            )
          )
          /
          3600
      - record: gitlab_com:last_walg_successful_basebackup_age_in_hours
        expr: |
          min without (fqdn,instance) (
            time()
            -
            (
              gitlab_job_success_timestamp_seconds{resource="walg-basebackup", env="gstg"} > 0
            )
          )
          /
          3600
      - record: gitlab_com:last_walg_failed_basebackup_age_in_hours
        expr: min(time()-push_time_seconds{job='walg-basebackup',status="-1", env="gstg"}) by (environment,type) / 3600
      - alert: walgBackupDelayed
        expr: (gitlab_com:last_walg_backup_age_in_seconds{type!="patroni-embedding", env="gstg"} >= 60 * 15) or (gitlab_com:last_walg_backup_age_in_seconds{type="patroni-embedding", env="gstg"} >= 60 * 60)
        for: 5m
        labels:
          pager: pagerduty
          severity: s1
          alert_type: symptom
        annotations:
          grafana_datasource_id: mimir-gitlab-gstg
          description:
            WAL-G wal-push archiving WALs to GCS might be not working. Please follow the runbook
            to review the problem.
          runbook: docs/patroni/postgresql-backups-wale-walg.md
          title: Last WAL was archived {{ .Value | humanizeDuration }} ago for env {{ $labels.environment }}.
      - alert: walgBaseBackupDelayed
        expr: gitlab_com:last_walg_successful_basebackup_age_in_hours{env="gstg"} >= 30
        for: 5m
        labels:
          pager: pagerduty
          severity: s1
          alert_type: symptom
        annotations:
          grafana_datasource_id: mimir-gitlab-gstg
          description:
            WAL-G backup-push creating full backups and archiving them to GCS might be not working. Please follow the runbook
            to review the problem.
          runbook: docs/patroni/alerts/walgBaseBackup.md
          title: Last successful WAL-G basebackup was seen {{ $value }} hours ago for env {{ $labels.environment }}.
      - alert: WALGBaseBackupFailed
        expr: gitlab_job_failed{resource="walg-basebackup", type!~".+logical.+", env="gstg"} == 1
        for: 5m
        labels:
          pager: pagerduty
          severity: s1
          alert_type: cause
        annotations:
          grafana_datasource_id: mimir-gitlab-gstg
          title: GitLab Job has failed
          description: >
            The GitLab job "{{ $labels.job}}" resource "{{ $labels.resource }}" has failed.
          runbook: docs/patroni/alerts/walgBaseBackup.md
