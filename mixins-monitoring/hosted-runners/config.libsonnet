{
  _config+:: {
    prometheusDatasource: 'Global',
    rateInterval: '5m',
    dashboardNamePrefix: 'Hosted Runners - ',
    dashboardTags: ['hosted-runner'],
  },
}
